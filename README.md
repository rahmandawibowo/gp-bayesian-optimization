# Bayesian Optimization Simulation

This project focuses on comparing three Bayesian Optimization's acquisition functions, [CEI](https://projecteuclid.org/ebooks/institute-of-mathematical-statistics-lecture-notes-monograph-series/new-developments-and-applications-in-experimental-design/Chapter/Global-versus-local-search-in-constrained-optimization-of-computer-models/10.1214/lnms/1215456182), [OOSS, and EI-OOSS](https://www.tandfonline.com/doi/abs/10.1080/10618600.2021.1935270?journalCode=ucgs20). The problem sets used for simulations are toy example, tension-compression spring problem and i-beam design problem.

## Project Structure

```
.
├── data/                                  // store simulation results in csv files
│   ├── ibeam/
│   │   └── <filename>.csv
│   ├── spring/
│   │   └── <filename>.csv
│   └── toy/
│       └── <filename>.csv
├── matlab/                                // simulation related codes
│   ├── dace/                              // dace kriging toolbox
│   ├── outputs/                           // store outputs from simulation
│   ├── utils/                             // utility functions
│   ├── acquisition_<cei|ooss|eiooss>.m    // acquisition functions
│   ├── main.m                             // main function
│   ├── main_<toy|spring|ibeam>.m          // main function for each problem set
│   └── simul_outputs_<toy|spring|ibeam>   // function for simulating outputs (objective + constraint)
├── analysis.R                             // code for analysis
└── README.md                              // docs
```

## Tools to used

Simulation uses Matlab, and analysis uses R. Several packages need to be installed before running the code:

**Matlab**
- Global Optimization Toolbox

**R**
- tidyverse (for plotting)
- rstatix (for pairwise testing)


## How to run

First clone this project to your local machine, and then you can follow the instruction.

### Simulation with Matlab

Open Matlab and set this project folder as a current folder in your Matlab. Then, open file `main.m` and click button `Run`.
To tweak the configuration specific to the problem set, you can open file `main_<toy|spring|ibeam>.m`, and change the parameters there before running. Because the simulation can take time, you can change the `macro` value to a small number.

To add a new problem set, you need to create two files:
- A `simul_outputs_<problem>.m` file to contain your objective and constraint functions.
- A function file with format `main_<problem>.m`. You can follow the coding structure from the existing main files.

To add a new acquisition function, you need to create a new function file with format `acquisition_<method>.m`, and then refer the new acquisition function in `main_<problem>.m`.

## Analysis with R

Open the `analysis.R` in Rstudio, and set your working directory using `setwd()`. Then, you can run the script normally.
