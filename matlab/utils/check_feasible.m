function [indf] = check_feasible(Winit)
%determines which points in Xnk are feasible (all constraints valid)

n=size(Winit,1); %nr of init design points
t=size(Winit,2); % nr of outputs (goal + constraints
indf=[];

    for i = 1:n %check for all points
      if Winit(i,2:t)<=0 % check for all constraints whether they are valid in point i
          % add point to index list
          indf= [indf; i];
      end
    end
    
end
            

