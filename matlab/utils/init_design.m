function Xnk_init = init_design(n,k,bounds)
%
% set of initial input combinations; LHS with midpoints (smooth off)
% https://nl.mathworks.com/help/stats/lhsdesign.html
Xnk_start = lhsdesign(n,k,'criterion','maximin','smooth','off');
 % initial design Xnk
Xnk_init=bounds(:,1)'+Xnk_start.*(bounds(:,2)-bounds(:,1))';
end

