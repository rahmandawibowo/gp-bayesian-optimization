function TrueBest=findbest(X, simul_outputs_fn)

%simulate all points in X and determine best feasible point
Y=simul_outputs_fn(X);
k=size(X,2);%nr of input variables
index=check_feasible(Y); % rows containing feasible points according to constraints
if size(index,1)>=1
    best=find(Y(index,1)==min(Y(index,1))); % find best point among the feasible points
    TrueBest=[X(index(best(1)),:) Y(index(best(1)),:)]; %store the data of the first point found (there can be mutiple points)
else %there is no feasible point in the list
TrueBest=10^6*ones(1,k+size(Y,2));
end

