clear; %clc;
addpath('C:\Users\Rahmanda Wibowo\Documents\master\semester 4\thesis\dace\');
addpath('C:\Users\Rahmanda Wibowo\Documents\master\semester 4\thesis\pourmohamad\toy problem\');

rng(1);

macrorep = 100;

p = [];

for i=1:macrorep
    n = 100000;
    k = 2;
    bounds=[0 1;0 1];
    
    x = init_design(n,k,bounds);
    w = simul(x);
    w_feasible = check_feasible(w);
    p = [p; size(w_feasible,1) / n];
end