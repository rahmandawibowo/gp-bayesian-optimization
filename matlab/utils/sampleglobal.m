function Xcand = sampleglobal(nc,k,bounds)
%
% set of initial input combinations; Latin hypercube design with midpoints (smooth off)
X = lhsdesign(nc,k,'criterion','maximin','smooth','off');
 % initial design Xnk
Xcand=bounds(:,1)'+X.*(bounds(:,2)-bounds(:,1))';
end

