% This visualization is only for toy problem
function viz_optim(simul_outputs_fn, macrorep, all_infill, all_init_design, all_x_candidates, overviewconv_permacrorep, true_best_x, dir)
% replicate visualization of toy problem Gramacy et al. (2016)
[X,Y] = meshgrid(0:.005:1,0:.005:1);
X1 = reshape(X.',[], 1);
X2 = reshape(Y.', [], 1);
outputs = simul_outputs_fn([X1, X2]);
const1 = outputs(:, 2) <= 0;
const2 = outputs(:, 3) <= 0;
X1(~(const1&const2)) = NaN;
X2(~(const1&const2)) = NaN;
Z = outputs(:, 1);
W0 = reshape(Z, size(X));
X = reshape(X1, size(W0));
Y = reshape(X2, size(W0));

filtered_all_infill = all_infill(all_infill(:, 1) == macrorep, :);
filtered_all_init_design = all_init_design(all_init_design(:, 1) == macrorep, :);
filtered_all_x_candidates = all_x_candidates(all_x_candidates(:, 1) == macrorep, :);
filtered_overviewconv = overviewconv_permacrorep(macrorep, :);

f = figure(macrorep);

% Plot the surface from true function
contourf(X, Y, W0, 0, 'FaceColor', '#F0F0F0')

% Plot the initial design points
hold on
scatter(filtered_all_init_design(:, 2), filtered_all_init_design(:, 3), 'mo')
hold off

% Plot all candidate points
%hold on
%scatter(filtered_all_x_candidates(:, 2), filtered_all_x_candidates(:, 3), 'rd')
%hold off

% Plot all infill points
hold on
scatter(filtered_all_infill(:, 2), filtered_all_infill(:, 3), 75, 'k.')
hold off

% Plot the best point from optimization
hold on
scatter(filtered_overviewconv(1), filtered_overviewconv(2), 75, 'r*');
hold off

% Plot the true best
hold on
scatter(true_best_x(1), true_best_x(2), 'b*');
hold off

% Uncomment to see the infill iteration number
%hold on
%text(filtered_all_infill(:, 2) + 0.01, filtered_all_infill(:, 3), string(1:size(filtered_all_infill, 1)), 'FontSize', 8);
%hold off

xlabel('x1')
ylabel('x2')
legend('', 'initial design', 'infill points', 'optim best', 'true best', 'Location', 'southeastoutside');
legend boxoff;
set(f, 'Position', [100 100 500 300]); % good for legend
%set(f, 'Position', [100 100 365 300]); % good for legend

folder_path = strcat(dir, 'figures');
mkdir(folder_path);
image_path = @(format) strcat(folder_path, '/macrorep-', string(macrorep), '-scatter-plot', format);
saveas(f, image_path('.fig'));
saveas(f, image_path('.svg'));
%exportgraphics(f, image_path('.png'),'Resolution',300)
end