bounds = [10 80;10 50;0.9 5;0.9 5]; % bounds of the X inputs
nstart = 20;                        % nr of starting points for multistart infill criterion optimization
q = 3;                              % number of output types (object plus constraints)
macro = 100;                        % total number of macro replications
totobs = 120;                       % total number of expensive simulations allowed
TrueBestX= [80 50 0.9 2.32]; % true best x inputs
initial_benchmark = 100000;

% CEI
simulation(bounds, nstart, q, macro, totobs, TrueBestX, initial_benchmark, @simul_outputs_ibeam, @acquisition_cei, 'outputs/ibeam/cei/')
% OOSS
% simulation(bounds, nstart, q, macro, totobs, TrueBestX, initial_benchmark, @simul_outputs_ibeam, @acquisition_ooss, 'outputs/ibeam/ooss/')
% EI-OOSS
% simulation(bounds, nstart, q, macro, totobs, TrueBestX, initial_benchmark, @simul_outputs_ibeam, @acquisition_eiooss, 'outputs/ibeam/ei-ooss/')