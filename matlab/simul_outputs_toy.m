function [Wh] = simul_outputs_toy(Xnk)
x = Xnk(:,1);
y = Xnk(:,2);

w0 = sum(Xnk,2);
w1 = 1.5-x-2*y-(0.5).*sin(2*pi*(x.^2-2*y));
w2 = x.^2+y.^2 -3/2;

Wh = [w0 w1 w2];
end
