bounds = [0 1;0 1];          % bounds of the X inputs
nstart = 20;                 % nr of starting points for multistart infill criterion optimization
q = 3;                       % number of output types (object plus constraints)
macro = 100;                 % total number of macro replications
totobs = 120;                % total number of expensive simulations allowed
TrueBestX= [0.1954 0.4044];  % true best x inputs
initial_benchmark = 0;

% CEI
simulation(bounds, nstart, q, macro, totobs, TrueBestX, initial_benchmark, @simul_outputs_toy, @acquisition_cei, 'outputs/toy/cei/')
% OOSS
% simulation(bounds, nstart, q, macro, totobs, TrueBestX, initial_benchmark, @simul_outputs_toy, @acquisition_ooss, 'outputs/toy/ooss/')
% EI-OOSS
% simulation(bounds, nstart, q, macro, totobs, TrueBestX, initial_benchmark, @simul_outputs_toy, @acquisition_eiooss, 'outputs/toy/ei-ooss/')