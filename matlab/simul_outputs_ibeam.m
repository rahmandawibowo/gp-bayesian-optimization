function [Wh] = simul_outputs_ibeam(Xnk)
x1 = Xnk(:, 1);
x2 = Xnk(:, 2);
x3 = Xnk(:, 3);
x4 = Xnk(:, 4);

t1w0 = 1/12*x3.*(x1 - 2*x4).^3 + 1/6*x2.*(x4.^3) + 2*x2.*x4.*((x1 - x4)./2).^2;
w0 = 5000 ./ t1w0;

w1 = 2*x2.*x4 + x3.*(x1 - 2*x4) - 300;

t1w2 = 180000*x1 ./ (x3.*(x1 - 2*x4).^3 + 2*x2.*x4.*(4*x4.^2+3*x1.*(x1 - 2*x4)));
t2w2 = 15000*x2 ./ ((x1 - 2*x4).*x3.^3 + 2*x4.*x2.^3);
w2 = t1w2 + t2w2 - 6;

Wh = [w0 w1 w2];
end