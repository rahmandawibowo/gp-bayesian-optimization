function [] = simulation(bounds, nstart, q, macro, totobs, TrueBestX, initial_benchmark, simul_outputs_fn, acquisition_fn, outputs_dir)
addpath('utils');
addpath('dace');

findbest_outputs = @(x) findbest(x, simul_outputs_fn);

k = size(bounds,1); % number of inputs
n = (k + 1)*(k +2 )/2; % Tao (number of initial design)
maxiter=totobs-n; % max number of infill points allowed 
Xrepeat = n; % max number of points that is chosen from already available points in design

TrueBestY=simul_outputs_fn(TrueBestX); 
TrueBest=[TrueBestX TrueBestY];

% initialize Kriging parameters (theta)
theta0 =   1*ones(1,k); % initial guess for theta
lob = 0.001* ones(1,k); % lower bound Kriging parameters (thetas)
upb =      5*ones(1,k); % upper bound Kriging parameters (thetas)

% for outputs
overviewbest_permacrorep=zeros(maxiter+1,macro); % to store the best points resulting from each macrorep
overviewconv_permacrorep=[];

% for debugging
all_infill = []; % k-iteration of macrorep, x1, x2, ..., w0, w1, ...
all_init_design = []; % k-iteration of macrorep, x1, x2, ...
all_x_candidates = []; % k-iteration of macrorep, x1, x2, ...

for macrorep=1:macro
    
    macrorep;
    rng((macrorep-1)*30000,'twister');  % set random number generator state (seed)
    
    % determine initial design and feasibility
    Xinit = init_design(n,k,bounds); % n initial design points in k dimensions
    all_init_design = [all_init_design; [ones(n,1)*macrorep Xinit]];
    Winit = simul_outputs_fn(Xinit); %simulate outputs for Xnk: col 1 = goal values, col2-t = constraints;
    stopflag=0; % checks whether stopping condition is satisfied
    overviewbest=[]; % matrix to store the consecutive best points found (infill points are added only when feasible and effectively better)
    iteration=0;
    
 
    while stopflag==0 % while the iterations have not yet reached the stopping criterion
    
    iteration=iteration+1;
    %determine kriging model per output 
    Dmodelh=[]; Ych=[]; Ych_s2=[]; index=[];
        
    % building surrogate model using GP
    for h = 1:q
        [dmodelt, perft] = dacefit(Xinit,Winit(:,h),@regpoly0,@corrgauss,theta0,lob,upb);
        Dmodelh = [Dmodelh dmodelt]; %keep model parameters for each output       
    end

    %determine Xstart = matrix with the multistart points for infill
    %optimization
    %Xstart0=sampleglobal(10*k,k,bounds); %generate lhs design for starting points for fmincom, 10*k is from dJones 1998
    %all_x_candidates = [all_x_candidates; [ones(10*k,1)*macrorep Xstart0]];
    if and(iteration-1>=1,iteration-1<Xrepeat) % if you have at least 1 infill point extra already, but not enough to select Xrepeat points
       Xstartover=Xinit(n+1:size(Xinit,1),:); %points that will be reused as starting points = most recent ones most recent ones 
    elseif iteration-1>=Xrepeat % you have enough iterations done already to have all starting points selected from last points of the design matrix
       Xstartover=Xinit(size(Xinit,1)-(Xrepeat-1):size(Xinit,1),:)
    else
       Xstartover=[]
    end

    Xstart0=sampleglobal(nstart-size(Xstartover,1),k,bounds); %generate lhs design for starting points for pattern search
    all_x_candidates = [all_x_candidates; [ones(nstart-size(Xstartover,1),1)*macrorep Xstart0]];
    
    Xstart=[Xstart0;Xstartover];

    
    %determine fmin
    best=findbest_outputs(Xinit);
    overviewbest=[overviewbest;best]; % keep track of best points so far(Xvalues Wvalues)
    fmin=best(:,k+1); % best goal function value at this moment

    
    flagalpha1=0;
    infill=[];
    
    while flagalpha1==0 % while no infill point found

    lb=bounds(:,1);
    ub=bounds(:,2);
    fun=@(x)acquisition_fn(x,Dmodelh,fmin);
    options = optimoptions('patternsearch','PollMethod','GSSPositiveBasis2N','MeshTolerance',0.0001);
    nonlcon=[];
    
    benchmark=initial_benchmark;
         
    for prob=1:size(Xstart0,1) % use all these points as starting points for the infill optimizer
        fprintf("macrorep %i infill %i\n", macrorep, prob)
        x0 = Xstart0(prob,:); % choose this point as starting point
        [x,fval,exitflag,output] = patternsearch(fun,x0,[],[],[],[],lb,ub,nonlcon,options);

        if and(fval<benchmark, size([Xinit;x],1)==size(uniquetol([Xinit;x],'ByRows',true),1)) % new infill point found
            % check if infill point is already in Xinit before accepting it
            infill=x;
            benchmark=fval;
        end
    
    end
    
    
    if (isempty(infill)==0) % if the iteration yielded an infill point
        Xinit=[Xinit;infill]; % add the new infill point to the initial design points for the next iteration
        Winfill=simul_outputs_fn(infill);
        Winit=[Winit;Winfill]; % add outputs to the initial design points for the next iteration
        all_infill=[all_infill;[macrorep infill Winfill]];
        flagalpha1=1;
        if iteration==maxiter % check if you have reached maximum evaluation budget
            stopflag=1;
        end
    else % if none of the prob points yielded an infill point
        stopflag=1;
        flagalpha1=1;
    end

    end
    
    end
    
conv=overviewbest(size(overviewbest,1),:); % final goal value that is put forward as best by optimum
overviewbest=[overviewbest;zeros(totobs-size(overviewbest,1),size(overviewbest,2))]; % finish looping and show best points for this macrorep; 
%last row is zeros to indicate stopping criterion was met
matrix=['save(',char(39),outputs_dir,'overviewbest_macro',int2str(macrorep),char(39),',',char(39),'overviewbest',char(39),');'];
eval(matrix);

overviewbest_permacrorep(1:size(overviewbest,1),macrorep)=overviewbest(:,k+1); % keep track of overviewbest for each macrorep in separate matrix
% keep track of overviewbest for each macrorep in separate matrix
matrix=['save(',char(39),outputs_dir,'overviewbest_permacrorep.mat',char(39),',',char(39),'overviewbest_permacrorep',char(39),');'];
eval(matrix);

overviewconv_permacrorep=[overviewconv_permacrorep;conv];
matrix=['save(',char(39),outputs_dir,'overviewconv.mat',char(39),',',char(39),'overviewconv_permacrorep',char(39),');'];
eval(matrix);
end

% visualization only apply for toy problem
if macrorep > 2 && k == 2
    for fig=1:3
        viz_optim(simul_outputs_fn, fig, all_infill, all_init_design, all_x_candidates, overviewconv_permacrorep, TrueBestX, outputs_dir);
    end
end
end