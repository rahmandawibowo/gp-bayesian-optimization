function a=acquisition_eiooss(X,Dmodelh,fmin)
[yhat, dyhat, mse, dmse] = predictor(X, Dmodelh(1));
[yw1, dyw1, msew1, dmsew1] = predictor(X, Dmodelh(2));
[yw2, dyw2, msew2, dmsew2] = predictor(X, Dmodelh(3));

if and(yw1 < 0, yw2 < 0)
    lw1 = log(-yw1)
    lw2 = log(-yw2)
    bw1 = lw1 + (msew1 / (2*(yw1^2)));
    bw2 = lw2 + (msew2 / (2*(yw2^2)));
    diff= fmin-yhat;
    sigma=sqrt(mse);
    EI=diff.*normcdf(diff./sigma,0,1)+sigma.*normpdf(diff./sigma,0,1);
    a =-EI - mse * (bw1 + bw2);
else
    a = Inf
end
end
