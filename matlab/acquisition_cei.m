function CEI=acquisition_cei(X,Dmodelh,fmin)
[yhat, dyhat, mse, dmse] = predictor(X, Dmodelh(1));
[yw1, dyw1, msew1, dmsew1] = predictor(X, Dmodelh(2));
[yw2, dyw2, msew2, dmsew2] = predictor(X, Dmodelh(3));
% calculate expectation of improvement
s = sqrt(mse); % standard deviation of the predictive distribution
EI = (fmin-yhat)*normcdf((fmin-yhat)/s) + s*normpdf((fmin-yhat)/s);

pw1 = normcdf(0, yw1, sqrt(msew1));
pw2 = normcdf(0, yw2, sqrt(msew2));

CEI = -EI * pw1 * pw2; % CEI = EI * P(w1(x) <= 0) * P(w2(x) <= 0)
end
