bounds = [2 15;0.25 1.30;0.05 0.20];    % bounds of the X inputs
nstart = 20;                            % nr of starting points for multistart infill criterion optimization
q = 5;                                  % number of output types (object plus constraints)
macro = 100;                            % total number of macro replications
totobs = 120;                           % total number of expensive simulations allowed
TrueBestX= [11.287126 0.35675 0.05169]; % true best x inputs
initial_benchmark = 100000;

% CEI
simulation(bounds, nstart, q, macro, totobs, TrueBestX, initial_benchmark, @simul_outputs_spring, @acquisition_cei, 'outputs/spring/cei/')
% OOSS
% simulation(bounds, nstart, q, macro, totobs, TrueBestX, initial_benchmark, @simul_outputs_spring, @acquisition_ooss, 'outputs/spring/ooss/')
% EI-OOSS
% simulation(bounds, nstart, q, macro, totobs, TrueBestX, initial_benchmark, @simul_outputs_spring, @acquisition_eiooss, 'outputs/spring/ei-ooss/')